package top.mybiao.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import top.mybiao.redis.core.DistributeLock;
import top.mybiao.redis.core.RedisLockFactory;

import javax.annotation.PostConstruct;
import java.util.Objects;

@SpringBootApplication
@RestController
public class RedisApplication {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RedisLockFactory redisLockFactory;

    private static final Logger log = LoggerFactory.getLogger(RedisApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }

    @PostMapping("/seal")
    public String seal(String name){
        DistributeLock lock = redisLockFactory.getLock("seal");
        try{
            lock.lock();
            log.info("name={},want seal 1 iphone",name);
            String num = redisTemplate.opsForValue().get("iphone");
            Thread.sleep(1000);
            if (Objects.nonNull(num) && Integer.parseInt(num)>0) {
                redisTemplate.opsForValue().increment("iphone", -1);
                log.info("buy one iphone success");
                return "success";
            }
            log.info("库存不足");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return "系统繁忙";
    }


    @PostConstruct
    public void init(){
        redisTemplate.opsForValue().set("iphone","100");
    }

}
