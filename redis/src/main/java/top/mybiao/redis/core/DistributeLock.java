package top.mybiao.redis.core;

import java.util.concurrent.TimeUnit;

public interface DistributeLock {
    boolean tryLock();

    boolean tryLock( long time, TimeUnit timeUnit);

    void lock();

    void unlock();
}
