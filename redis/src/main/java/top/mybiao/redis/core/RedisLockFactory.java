package top.mybiao.redis.core;

public interface RedisLockFactory {

    DistributeLock getLock(String key);
}
