package top.mybiao.redis.core;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;

import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class RedisLockImpl implements DistributeLock{

    private final StringRedisTemplate redisTemplate;

    private final String key;

    private final String lockKey;

    //过期时间30秒
    private static final int TIMEOUT = 30;

    public RedisLockImpl(StringRedisTemplate redisTemplate, String key,String lockKey){
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.lockKey = lockKey;
        System.out.println("初始化lockKey:"+lockKey);
    }
    private static final String lockScript = "" +
            "local key=KEYS[1]\n" +
            "local lockKey=ARGV[1]\n" +
            "local expireSecond=ARGV[2]\n" +
            "local exists=redis.call('exists',key)\n" +
            "if exists==0 then\n" +
            "   redis.call('hset',key,lockKey,1)\n" +
            "   redis.call('expire',key,expireSecond)\n" +
            "   return 1\n" +
            "end\n" +
            "local value=redis.call('hget',key,lockKey)\n" +
            "if value then \n" +
            "   redis.call('hincrby',key,lockKey,1)\n" +
            "   redis.call('expire',key,expireSecond)\n" +
            "   return 1\n" +
            "end\n" +
            "return 0\n";
    private static final String unlockScript = "" +
            "local key=KEYS[1]\n" +
            "local lockKey=ARGV[1]\n"  +
            "local value=redis.call('hget',key,lockKey)\n" +
            "if value then\n" +
            "   if tonumber(value)>1 then\n" +
            "       redis.call('hincrby',key,lockKey,-1)\n" +
            "   else\n" +
            "       redis.call('del',key)\n" +
            "   end\n" +
            "end\n" +
            "return 1";

    private final RedisScript<Long> lockRedisScript = new DefaultRedisScript<>(lockScript,Long.class);
    private final RedisScript<Long> unlockRedisScript = new DefaultRedisScript<>(unlockScript,Long.class);

    @Override
    public boolean tryLock() {
        Long code = redisTemplate.execute(lockRedisScript, Collections.singletonList(key),lockKey,String.valueOf(TIMEOUT));
        Objects.requireNonNull(code);
        return code==1;
    }

    @Override
    public boolean tryLock(long time, TimeUnit timeUnit) {
        if (time<=0) throw new IllegalArgumentException("time must greater than zero");
        long start = System.currentTimeMillis();
        long end = start+timeUnit.toMillis(time);
        try {
            while (start < end) {
                if(tryLock()) return true;
                Thread.sleep(10);
                start = System.currentTimeMillis();
            }
        }catch (InterruptedException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        return false;
    }

    @Override
    public void lock() {
        try {
            while (!tryLock()) {
                Thread.sleep(30);
            }
            System.out.println("获取到锁");
        }catch (InterruptedException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void unlock() {
        System.out.println("释放锁");
        redisTemplate.execute(unlockRedisScript,Collections.singletonList(key),lockKey);
    }
}
