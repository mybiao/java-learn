package top.mybiao.redis.core;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RedisLockFactoryImpl implements RedisLockFactory{

    private final StringRedisTemplate redisTemplate;

    public RedisLockFactoryImpl(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public DistributeLock getLock(String key) {
        return new RedisLockImpl(redisTemplate,key, UUID.randomUUID().toString());
    }
}
